FROM openjdk:8-jdk-alpine
MAINTAINER Doyatama <1941720030@student.polinema.ac.id>
RUN mkdir -p /app
WORKDIR /app

COPY LoopDito.java /app

RUN javac LoopDito.java

CMD ["java", "LoopDito"]
#compile file java
#ENTRYPOINT ["javac","LoopDito.java"]

#running java
#ENTRYPOINT ["java","LoopDito"]
